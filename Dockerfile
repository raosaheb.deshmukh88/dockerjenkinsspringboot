FROM openjdk:8
expose 8082
ADD target/DockerJenkinsSpringBoot-0.0.1-SNAPSHOT.jar dockerjenkinsspring-boot.jar
ENTRYPOINT ["java",".jar","/dockerjenkinsspring-boot.jar"]